package br.com.olx.java.main;

import java.io.Serializable;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.w3c.dom.Document;

import br.com.olx.java.model.Property;
import br.com.olx.java.model.Xml;
import br.com.olx.java.rest.HttpBin;
import br.com.olx.java.util.Messages;
import br.com.olx.java.util.StdOut;
import br.com.olx.java.util.Streaming;


public class Main implements Serializable{
	
	private static final long serialVersionUID = -3757393226835122844L;
	
	
	public static void main(String[] args) {
		StdOut.println();StdOut.println(Messages.INICIADO);
		Calendar dtIni = Calendar.getInstance();
		try{  
		
			URL url = Streaming.createURL(Messages.XML_URL);
			
			Document doc = Streaming.openStream(url);
			
			StdOut.println(Messages.INICIO_CONVERSAO);
			List<Property> properties = Xml.toJson(doc);
			
			StdOut.println();StdOut.println(Messages.CONVERSAO_SUCESSO);
			StdOut.println(Messages.INI_POST);StdOut.println();
				
			HttpBin.doPost(properties);
			
			StdOut.println();StdOut.println(Messages.FIM_POST);
			StdOut.println();StdOut.println(Messages.FINALIZADO);
			StdOut.println();StdOut.println(properties.size() + Messages.PROCESSADO_EM + (Calendar.getInstance().getTimeInMillis() - dtIni.getTimeInMillis()) +" ms.");
			
		}catch(Exception e){
			e.printStackTrace();
			StdOut.println(Messages.PROBLEMA_INESPERADO);
		}
	}

}
