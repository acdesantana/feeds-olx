package br.com.olx.java.util;

public class Messages {

	public static final String FILE_PATH = "file://";
	public static final String FILE_SEPARATOR = "file.separator";
	public static final String WS_URL = "http://httpbin.org/post"; 
	public static final String XML_URL = "http://ahul.github.io/imoveis.xml";
	public static final String INICIO_CONVERSAO = "Iniciando a leitura do XML...";
	public static final String CONVERSAO_SUCESSO = "Leitura e conversão realizadas com sucesso.";
	public static final String NAO_ENCONTROU_ARQUIVO = "Não foi possível encontrar o arquivo: ";
	public static final String PROBLEMA_INESPERADO = "Ocorreu um problema inesperado.";
	public static final String XML_MAL_FORMADO = "XML mal formado. Não é possível gerar o JSON.";
	public static final String JSON_ERRO_CONEXAO_HTTP = "JSON - Erro conexão HTTP ";
	
	public static final String HTTPBIN_ECHO = "HttpBin echo:";
	public static final String INI_POST = "Iniciando POST dos recursos...";
	public static final String FIM_POST = "POST dos recursos finalizado.";
	public static final String INICIADO = "Desafio Feeds OLX iniciado.";
	public static final String FINALIZADO = "Desafio finalizado com sucesso.";
	public static final String PROCESSADO_EM = " registros processados em ";
	
	public static final String EMPTY = "";
	public static final String ENTER = "\n";
	public static final String IMOVEL = "Imovel";
	public static final String COD_IMOVEL = "CodigoImovel";
	public static final String TIPO_IMOVEL = "TipoImovel";
	public static final String OBS = "Observacao";
	public static final String CIDADE= "Cidade";
	public static final String BAIRRO = "Bairro";
	public static final String NUMERO = "Numero";
	public static final String COMPLEM = "Complemento";
	public static final String CEP = "CEP";
	public static final String URL_ARQ= "URLArquivo";
	
	public static final String ATTR_PROP_CODE = "propertyCode";
	public static final String ATTR_PROP_TYPE = "propertyType";
	public static final String ATTR_DESCRIPTION = "description";
	public static final String ATTR_UPDATE = "updatedAt";
	public static final String ATTR_ADDRESS = "address";
	public static final String ATTR_CITY = "city";
	public static final String ATTR_NEIGHBOURHOOD = "neighbourhood";
	public static final String ATTR_NUMBER = "number";
	public static final String ATTR_COMPL = "complement";
	public static final String ATTR_ZIPCODE = "zipCode";
	public static final String ATTR_PHOTOS = "photos";
	public static final String ATTR_URL = "url";
	
	public static final String DD_MM_YYYY = "dd/MM/yyyy";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	
}
