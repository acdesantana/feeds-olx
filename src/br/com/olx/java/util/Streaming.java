package br.com.olx.java.util;

import java.io.File;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

public class Streaming implements Serializable {

	private static final long serialVersionUID = -1551188082118087749L;

	
	public static URL createURL(String fileName){
	   URL url = null;
	   try {
	      url = new URL(fileName);
	
	   }catch (MalformedURLException ex){
	      File f = new File(fileName);
	      try {
	         String path = f.getAbsolutePath();
	         String fs = System.getProperty(Messages.FILE_SEPARATOR);
	         if (fs.length() == 1){
	            char sep = fs.charAt(0);
	            if (sep != '/'){
	               path = path.replace(sep, '/');
	            }
	            
	            if (path.charAt(0) != '/'){
	               path = '/' + path;
	         	}
	         }
	         path = Messages.FILE_PATH + path;
	         url = new URL(path);
	      
	      }catch (MalformedURLException e){
	         StdOut.println(Messages.NAO_ENCONTROU_ARQUIVO + fileName);
	         System.exit(0);
	      }
	   }
	   return url;
	}

	
	public static Document openStream(URL url) throws Exception{
		Document doc = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(url.openStream());
		
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}
		
		return doc;
	}

	
}
