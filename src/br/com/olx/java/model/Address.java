package br.com.olx.java.model;

import java.io.Serializable;

public class Address  implements Serializable{

	private static final long serialVersionUID = 1441697412838557739L;
	
	private String city;
	private String neighbourhood;
	private String number;
	private String complement;
	private String zipCode;
	
	public Address(){ }
	public Address(String city, String neighbourhood, String number, String complement, String zipCode){
		this.city = city;
		this.neighbourhood = neighbourhood;
		this.number = number;
		this.complement = complement;
		this.zipCode = zipCode;
	}
	
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getNeighbourhood() {
		return neighbourhood;
	}
	public void setNeighbourhood(String neighbourhood) {
		this.neighbourhood = neighbourhood;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getComplement() {
		return complement;
	}
	public void setComplement(String complement) {
		this.complement = complement;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
}
