package br.com.olx.java.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.olx.java.util.Messages;

public class Property implements Serializable {

	private static final long serialVersionUID = 7300633694061976424L;

	
	private String code;
	private String type;
	private String description;
	private String updateDate;
	private Address address;
	private List<Photo> photos;
	
	
	public Property(Address address, List<Photo> photos){
		this.address = address;
		this.photos = photos;
	}
	
	public Property(String code, String type, String description, String updateDate, Address address, List<Photo> photos){
		this.code = code;
		this.type = type;
		this.description = description;
		this.updateDate = updateDate;
		this.address = address;
		this.photos = photos;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUpdateDate() {
		String date = this.updateDate;
		if(null != updateDate && !updateDate.isEmpty()){
			SimpleDateFormat formato = new SimpleDateFormat(Messages.DD_MM_YYYY);
			Date d = null;
			try {
				d = formato.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			formato.applyPattern(Messages.YYYY_MM_DD);
			date = formato.format(d);
		}
		return date;
	}
	public void setUpdateDate(String updateDate) {
		String date = this.updateDate;
		if(null != date && !date.isEmpty()){
			SimpleDateFormat formato = new SimpleDateFormat(Messages.YYYY_MM_DD);
			Date d = null;
			try {
				d = formato.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			formato.applyPattern(Messages.DD_MM_YYYY);
			date = formato.format(d);
		}
		this.updateDate = date;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<Photo> getPhotos() {
		return photos;
	}
	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}
	
}
