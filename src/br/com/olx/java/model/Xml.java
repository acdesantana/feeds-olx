package br.com.olx.java.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import br.com.olx.java.util.Messages;
import br.com.olx.java.util.StdOut;



public class Xml implements Serializable{

	private static final long serialVersionUID = -6162037183340031422L;
	
	public static List<Property> toJson(Document doc) throws Exception{

		Property property = null;
		Photo photo = null;
		Address address = null;
		List<Photo> photos = null;
		List<Property> properties = new ArrayList<Property>();
		
		NodeList nl = doc.getElementsByTagName("*");   
		int nodeListSize = nl.getLength();
		
		if(3 >= nodeListSize){
			StdOut.println(Messages.XML_MAL_FORMADO);
			throw new Exception(Messages.XML_MAL_FORMADO);
		
		}else{

			for (int j=2; j < nodeListSize; j++){
				
				Element e = (Element)nl.item(j);
				Node item = e.getChildNodes().item(0);
			
				String tagName = e.getTagName();
				String tagValue = (null == item)? Messages.EMPTY : (null == item.getNodeValue() || item.getNodeValue().contains(Messages.ENTER))? Messages.EMPTY : item.getNodeValue();
		
				
				if(Messages.IMOVEL.equalsIgnoreCase(tagName)){
					address = new Address();
					photos = new ArrayList<Photo>();
					property = new Property(address, photos);
					properties.add(property);
					continue;
				
				}else if(Messages.COD_IMOVEL.equalsIgnoreCase(tagName)){
					property.setCode(tagValue);
				
				}else if(Messages.TIPO_IMOVEL.equalsIgnoreCase(tagName)){
					property.setType(tagValue);
				
				}else if(Messages.OBS.equalsIgnoreCase(tagName)){
					property.setDescription(tagValue.isEmpty()? tagValue : tagValue.substring(26));
					property.setUpdateDate(tagValue.isEmpty()? tagValue : tagValue.substring(14, 24));
				
				}else if(Messages.CIDADE.equalsIgnoreCase(tagName)){
					address.setCity(tagValue);
				
				}else if(Messages.BAIRRO.equalsIgnoreCase(tagName)){
					address.setNeighbourhood(tagValue);
				
				}else if(Messages.NUMERO.equalsIgnoreCase(tagName)){	
					address.setNumber(tagValue);
				
				}else if(Messages.COMPLEM.equalsIgnoreCase(tagName)){
					address.setComplement(tagValue);
				
				}else if(Messages.CEP.equalsIgnoreCase(tagName)){
					address.setZipCode(tagValue);
				
				}else if(Messages.URL_ARQ.equalsIgnoreCase(tagName)){
					photo = new Photo();
					photo.setUrl(tagValue);
					photos.add(photo);
				}
			}
		}
		return properties;
	}
	
}
