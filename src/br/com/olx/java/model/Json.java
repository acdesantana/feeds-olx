package br.com.olx.java.model;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import br.com.olx.java.util.Messages;

public class Json implements Serializable {

	private static final long serialVersionUID = -303991148415420762L;

	
	
	public static JSONObject toJsonObject(Property property) throws Exception{
		
		JSONObject addressJson = convertAddressToJson(property);
		JSONArray photosArray = convertPhotosToJson(property);
		JSONObject propertyJson = convertPropertyToJson(property, addressJson, photosArray);

		StringWriter out = new StringWriter();
		propertyJson.writeJSONString(out);
		
		return propertyJson;
	}
	

	public static Property fromJsonObject(JSONObject obj)  throws Exception{
		
		List<Photo> photos = extractPhotosFromJson(obj);
		Address address = extractAddressFromJson(obj);
		
		String code = (String)obj.get(Messages.ATTR_PROP_CODE);
		String type = (String)obj.get(Messages.ATTR_PROP_TYPE);
		String description = (String)obj.get(Messages.ATTR_DESCRIPTION);
		String updateDate = (String)obj.get(Messages.ATTR_UPDATE);
		
		return new Property(code, type, description, updateDate, address, photos);
	}
	
	
	@SuppressWarnings("unchecked")
	private static JSONObject convertPropertyToJson(Property property, JSONObject addressJson, JSONArray photosArray) {
		JSONObject propertyJson = new JSONObject();
		propertyJson.put(Messages.ATTR_PROP_CODE, property.getCode());
		propertyJson.put(Messages.ATTR_PROP_TYPE, property.getType());
		propertyJson.put(Messages.ATTR_DESCRIPTION, property.getDescription());
		propertyJson.put(Messages.ATTR_UPDATE, property.getUpdateDate());
		propertyJson.put(Messages.ATTR_ADDRESS, addressJson);
		propertyJson.put(Messages.ATTR_PHOTOS, photosArray);
		
		return propertyJson;
	}


	@SuppressWarnings("unchecked")
	private static JSONArray convertPhotosToJson(Property property) {
		JSONArray photosArray = new JSONArray();
		for(int i=0; i < property.getPhotos().size(); i++){
			JSONObject photosJson = new JSONObject();
			photosJson.put(Messages.ATTR_URL, property.getPhotos().get(i).getUrl());
			photosArray.add(photosJson);
		}
		return photosArray;
	}

	
	@SuppressWarnings("unchecked")
	private static JSONObject convertAddressToJson(Property property) {
		JSONObject addressJson = new JSONObject();
		addressJson.put(Messages.ATTR_CITY, property.getAddress().getCity());
		addressJson.put(Messages.ATTR_NEIGHBOURHOOD, property.getAddress().getNeighbourhood());
		addressJson.put(Messages.ATTR_NUMBER, property.getAddress().getNumber());
		addressJson.put(Messages.ATTR_COMPL, property.getAddress().getComplement());
		addressJson.put(Messages.ATTR_ZIPCODE, property.getAddress().getZipCode());
		
		return addressJson;
	}
	
	
	private static Address extractAddressFromJson(JSONObject obj) {
		JSONObject addressJson = (JSONObject)obj.get(Messages.ATTR_ADDRESS);
		Address address = new Address((String)addressJson.get(Messages.ATTR_CITY), (String)addressJson.get(Messages.ATTR_NEIGHBOURHOOD), 
							(String)addressJson.get(Messages.ATTR_NUMBER), (String)addressJson.get(Messages.ATTR_COMPL), (String)addressJson.get(Messages.ATTR_ZIPCODE));
		return address;
	}

	
	private static List<Photo> extractPhotosFromJson(JSONObject obj) {
		List<Photo> photos = new ArrayList<Photo>();
		JSONArray photosJson = (JSONArray)obj.get(Messages.ATTR_PHOTOS);
		for(int i=0; i < photosJson.size(); i++){
			JSONObject photoJson = (JSONObject)photosJson.get(i);
			String url = (String)photoJson.get(Messages.ATTR_URL);
			photos.add(new Photo(url));
		}
		return photos;
	}
	
}
