package br.com.olx.java.model;

import java.io.Serializable;

public class Photo  implements Serializable{

	private static final long serialVersionUID = -6791288928776787378L;

	private String url;

	
	public Photo() { }
	public Photo(String url) {
		this.url = url;
	}


	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
