package br.com.olx.java.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.json.simple.JSONObject;

import br.com.olx.java.model.Property;
import br.com.olx.java.model.Json;
import br.com.olx.java.util.Messages;
import br.com.olx.java.util.StdOut;
import br.com.olx.java.util.Streaming;



public class HttpBin implements Serializable {

	private static final long serialVersionUID = -5786747224866705072L;
	
	
	
	public static void doPost(List<Property> properties) throws Exception{
		HttpURLConnection conn = null;
		try{
			StdOut.println();
			
			String baseUrl = Messages.WS_URL;
			URL endpoint = Streaming.createURL(baseUrl);
			
			int i=0;
			for(Property property : properties){
				i=i+1;
				
				conn = (HttpURLConnection) endpoint.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				
				JSONObject serializedJSON = Json.toJsonObject(property);
				
				OutputStream os = conn.getOutputStream();
				os.write(serializedJSON.toJSONString().getBytes());
				os.flush();
				
				if(HttpURLConnection.HTTP_OK != conn.getResponseCode()){
					throw new RuntimeException(Messages.JSON_ERRO_CONEXAO_HTTP + conn.getResponseCode() +" - "+ conn.getResponseMessage());
				}
				
				BufferedReader buffer = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String output = buffer.readLine();
				
				StdOut.println(i+" registros.");
				StdOut.println(Messages.HTTPBIN_ECHO);StdOut.println();
				while (null != (output = buffer.readLine())) {
					StdOut.println(output);
				}
				
				conn.disconnect();
			}	
			
		}catch(IOException e){
			if(null != conn){
				conn.disconnect();
			}
			throw e;
		}
	}
	
}
