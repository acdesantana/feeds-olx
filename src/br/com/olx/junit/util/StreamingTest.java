package br.com.olx.junit.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import br.com.olx.java.util.Messages;
import br.com.olx.java.util.Streaming;

public class StreamingTest {

	
	private static String fileName = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		fileName = "imoveis_short.xml";
	}
	
		
	@Test
	public void testCreateURL() {
		try {
			URL url = Streaming.createURL(fileName);
			assertNotNull(url);
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}


	@Test
	public void testOpenStream() {
		
		try {
			File f = new File(fileName);
		    String path = f.getAbsolutePath();
			URL url = new URL(Messages.FILE_PATH + path);
			
			Document doc = Streaming.openStream(url);
			assertNotNull(doc);
			assertNotNull(doc.getElementsByTagName("Imovel"));
			
		} catch (MalformedURLException mf) {
			fail(mf.getMessage());
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	
}
