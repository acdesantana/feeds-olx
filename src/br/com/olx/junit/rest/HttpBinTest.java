package br.com.olx.junit.rest;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.olx.java.model.Address;
import br.com.olx.java.model.Photo;
import br.com.olx.java.model.Property;
import br.com.olx.java.rest.HttpBin;

public class HttpBinTest {

	private static List<Property> properties = null;
	
	@Before
	public void setUp() throws Exception {
		
		try {
			List<Photo> photos = new ArrayList<Photo>();
			
			Property prop1 = new Property("763088", "type", "descr", "23/05/2016", new Address(), photos);
			Property prop2 = new Property("762994", "type", "descr", "23/05/2016", new Address(), photos);
			
			properties = new ArrayList<Property>();
			properties.add(prop1);
			properties.add(prop2);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


	@Test
	public void testDoPost() {
		try{
			HttpBin.doPost(properties);
			
		}catch (RuntimeException re){	
			fail("HTTP not OK");
			
		}catch(Exception e){
			fail(e.getMessage());
		}
	}

}
