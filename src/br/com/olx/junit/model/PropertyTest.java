package br.com.olx.junit.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.olx.java.model.Address;
import br.com.olx.java.model.Photo;
import br.com.olx.java.model.Property;

public class PropertyTest {

	@Test
	public void testGetUpdateDateDdMmYyyy() {
		try{
			List<Photo> photos = new ArrayList<Photo>();
			Property prop = new Property("763088", "type", "descr", "23/05/2016", new Address(), photos);
			
			String date = prop.getUpdateDate();
			assertEquals("2016-05-23", date);
			
		}catch(Exception e){
			fail(e.getMessage());
		}
	}
	
	
	@Test
	public void testGetUpdateDateYyyyMmDd() {
		try{
			List<Photo> photos = new ArrayList<Photo>();
			Property prop = new Property("762994", "type", "descr", "2016-05-23", new Address(), photos);
			
			prop.setUpdateDate("2016-05-23");
			
		}catch(Exception e){
			fail(e.getMessage());
		}
	}

}
