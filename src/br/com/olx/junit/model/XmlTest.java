package br.com.olx.junit.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import br.com.olx.java.model.Property;
import br.com.olx.java.model.Xml;
import br.com.olx.java.util.Messages;

public class XmlTest {

	
	private static URL url = null;
	private static String fileName = null;
	
	@Before
	public void setUp() throws Exception {
		fileName = "imoveis_short.xml";
		File f = new File(fileName);
	    String path = f.getAbsolutePath();
		url = new URL(Messages.FILE_PATH + path);
	}


	@Test
	public void testToJson() {
		try{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(url.openStream());
					
			List<Property> properties = Xml.toJson(doc);
			
			assertNotNull(properties);
			assertEquals(2, properties.size());
			assertEquals("763088", properties.get(0).getCode());
			assertEquals("762994", properties.get(1).getCode());
			
		}catch(Exception e){
			fail(e.getMessage());
		}
	}

}
